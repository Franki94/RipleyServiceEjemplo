﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    public class Repository
    {
        List<Products> productos = new List<Products>
        {
            new Products{ID = 1, Name= "Televisor", Category="Entretenimiento", Price = 150.5 },
            new Products{ID = 2, Name= "Computador", Category="Tecnologia", Price = 250.5 },
            new Products{ID = 3, Name= "Equipode Sonido", Category="Entretenimiento", Price = 100.5 },
            new Products{ID = 4, Name= "Audifonos", Category="Entretenimiento", Price = 50.5 },
            new Products{ID = 5, Name= "Cocina", Category="Hogar", Price = 350.5 },
            new Products{ID = 6, Name= "Lavadora", Category="Blanca", Price = 550.5 },
            new Products{ID = 7, Name= "Secadora", Category="Blanca", Price = 550.5 },
            new Products{ID = 8, Name= "LapTop", Category="Tecnologia", Price = 1150.5 },
            new Products{ID = 9, Name= "Refrigerador", Category="Hogar", Price = 1050.5 },
            new Products{ID = 10, Name= "Muebles", Category="Hogar", Price = 350.5 },
            new Products{ID = 11, Name= "Inodoro", Category="Baño", Price = 20.5 },
            new Products{ID = 12, Name= "Taladro", Category="Ferreteria", Price = 255.5 },
            new Products{ID = 13, Name= "Fierro", Category="Ferreteria", Price = 15.5 },
            new Products{ID = 14, Name= "Cama", Category="Hogar", Price = 150.5 },
            new Products{ID = 15, Name= "Mesa", Category="Hogar", Price = 150.5 }           
        };
        public List<Products> GetAllProducts()
        {
            return productos.OrderBy(p => p.Name).ToList();
        }

        public string GetSomeJsonText()
        {
            var data = JsonConvert.SerializeObject(productos);

            return data;
        }

        public Products GetProductByID(int id)
        {
            return productos.Where(p => p.ID == id).FirstOrDefault();
        }

    }
}
