﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad
{
    public class Products
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public string Category { get; set; }
    }
}
