﻿using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace RipleyServiceEjemplo
{
    public class PainTextAjaxResult : IHttpActionResult
    {
        HttpRequestMessage Request;
        string valueJson;
        public PainTextAjaxResult(HttpRequestMessage request, string valueJson)
        {
            this.valueJson = valueJson;
            this.Request = request;
        }

        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var products = new Repository();
            var responde = new HttpResponseMessage()
            {
                Content = new StringContent(valueJson,Encoding.UTF8,"application/json"),
                RequestMessage = Request,
            };
            return Task.FromResult(responde);
        }
    }
}