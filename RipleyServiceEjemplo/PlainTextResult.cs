﻿using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace RipleyServiceEjemplo
{
    //IhttpActionResult = "Constructor de respuestas HttpResponseMessage"
    //Para definir mejor la accion del metodo de resultado httpResponseMessage
    public class PlainTextResult : IHttpActionResult
    {
        string value;
        HttpRequestMessage Request;

        public PlainTextResult(string value, HttpRequestMessage request)
        {
            this.value = value;
            this.Request = request;
        }
      
        public Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            var responde = new HttpResponseMessage()
                {
                    Content = new StringContent(value,Encoding.ASCII,"application/json"),
                    RequestMessage = Request,          
                };
            return Task.FromResult(responde);
        }
    }
}