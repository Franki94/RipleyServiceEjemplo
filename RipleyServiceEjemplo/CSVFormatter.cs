﻿using Entidad;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace RipleyServiceEjemplo
{
    //Para indicar los formatos que expone
    //El formato se describe como tipo y subtipo
    //text/html     -  image/png    - application/json  - application/xml
    public class CSVFormatter : MediaTypeFormatter
    {
        //en el constructor debemos poner los typos MIME que soportaremos
        public CSVFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/csv"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/csv"));

            //Esto es opcional, para soportar otras codificaciones de caracteres tales como ´
            SupportedEncodings.Add(new UTF8Encoding(encoderShouldEmitUTF8Identifier: false));
            SupportedEncodings.Add(Encoding.GetEncoding("iso-8859-1"));
        }

        //Sobreescribimos los metodos CanReadTypee y CanWriteType
        //Para verificar si puede o no serializar los objetos
        public override bool CanReadType(Type type)
        {
            bool result;
            if (type == typeof(Products))
            {
                result = true;
            }
            else
            {
                TypeInfo enumerableType = typeof(IEnumerable<Products>).GetTypeInfo();
                result = enumerableType.IsAssignableFrom(type.GetTypeInfo());
            }
            return result;
        }

        public override bool CanWriteType(Type type)
        {
            bool result;

            if (type == typeof(Products))
            {
                result = true;
            }
            else
            {
                TypeInfo enumerableType = typeof(IEnumerable<Products>).GetTypeInfo();
                result = enumerableType.IsAssignableFrom(type.GetTypeInfo());
            }
            return result;
        }

        public override Task WriteToStreamAsync(Type type, object value,
            Stream writeStream, HttpContent content, TransportContext transportContext)
        {
            var ResultTask = Task.Factory.StartNew(() =>
            {
                Encoding effectiveEncoding = SelectCharacterEncoding(content.Headers);
                var writer = new StreamWriter(writeStream, effectiveEncoding);

                var products = value as IEnumerable<Products>;
                if (products != null)
                {
                    foreach (var p in products)
                    {
                        writer.WriteLine($"{p.ID}, {p.Name}, {p.Price}, {p.Category}");
                    }
                }
                else
                {
                    var p = value as Products;
                    if (p == null)
                    {
                        throw new InvalidOperationException("No es posible realidar la serializacion");
                    }

                    writer.WriteLine($"{p.ID}, {p.Name}, {p.Price}, {p.Category}");
                }

                writer.Flush();
            });

            return ResultTask;
        }

        public override Task<object> ReadFromStreamAsync(Type type, Stream readStream, 
            HttpContent content, IFormatterLogger formatterLogger)
        {
            var ResultTask = Task<object>.Factory.StartNew(() =>
            {
                List<Products> Products = new List<Entidad.Products>();
                using (StreamReader reader = new StreamReader(readStream))
                {
                    string Line;
                    while ((Line = reader.ReadLine()) != null)
                    {
                        var values = Line.Split(new string[] { "," },
                                StringSplitOptions.RemoveEmptyEntries);
                        Products.Add(new Products()
                        {
                            ID = int.Parse(values[0]),
                            Name = values[1],
                            Price = double.Parse(values[2]),
                            Category = values[3],
                        });
                    }
                }

                object Result;
                if (Products.Count == 1)
                {
                    Result = Products[0];
                }
                else
                {
                    Result = Products;                    
                }
                return Result;
            });

            return ResultTask;
        }
    }
}