﻿using Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RipleyServiceEjemplo.Controllers
{
    public class ProductsController : ApiController
    {
        Repository Repository = new Repository();

        //GET http://<servidor>/api/products  HTTP/1.1
        //public List<Products> GetAllProducts()
        //{
        //    return Repository.GetAllProducts();
        //}

        ////GET http://<servidor>/api/products/5  HTTP/1.1
        //public Products GetProductByID(int id)
        //{
        //    return Repository.GetProductByID(id);
        //}

        //no devuelve contenido
        //public void PutSomething()
        //{

        //}

        ////devuelve un httpresponsemessage con todos los productos
        //public HttpResponseMessage GetProduct()
        //{
        //    var products = Repository.GetAllProducts();

        //    HttpResponseMessage Response = Request.
        //        CreateResponse(HttpStatusCode.OK, products);

        //    //para que el contenido este vigente en cache por 20 minutos
        //    Response.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue()
        //    {
        //        MaxAge = TimeSpan.FromSeconds(1280)
        //    };
        //    return Response;
        //}

        public IHttpActionResult GetHttpActionResult()
        {
            var json = Repository.GetSomeJsonText();
            return new PlainTextResult(json, Request);
            //return new PlainTextResult("Hola mundo Api", Request);

        }

        //public IHttpActionResult GetHttpActionJsonResult()
        //{
        //    var json = Repository.GetSomeJsonText();
        //    return new PainTextAjaxResult(Request, json);
        //}

        //public IHttpActionResult Get(int id)
        //{
        //    var product = Repository.GetProductByID(id);
        //    if (product == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(product);
        //}

        //public IEnumerable<Products> GetAll()
        //{
        //    return Repository.GetAllProducts();
        //}
    }
}
